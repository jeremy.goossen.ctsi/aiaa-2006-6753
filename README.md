# AIAA 2006-6753: SGP4 Source Code

Vallado, David A., Paul Crawford, Richard Hujsak, and T.S. Kelso, "Revisiting
Spacetrack Report #3," presented at the AIAA/AAS Astrodynamics Specialist
Conference, Keystone, CO, 2006 August 21–24.

The AIAA 2006-6753 source code (with the exception of this file) is available
online as a zipped archive from [CelesTrak][main]. According to the
publication's [Frequently Asked Questions][faq]:

> There is no license associated with the code and you may use it for any
> purpose--personal or commercial--as you wish. We ask only that you include
> citations in your documentation and source code to show the source of the code
> and provide links to the [main page][main], to facilitate communications
> regarding any questions on the theory or source code.

[main]: <https://celestrak.org/publications/AIAA/2006-6753/>
        "CelesTrak: Publications [AIAA 2006-6753]"
[faq]:  <https://celestrak.org/publications/AIAA/2006-6753/faq.php>
        "CelesTrak: Publications [AIAA 2006-6753] - FAQ"
